import unittest 
from subtract_square_state import SubtractSquareState
from game_state_tree import *

class TestGrow(unittest.TestCase):
    def setUp(self):
        self.a0 = SubtractSquareState('p1', current_total = 0)
        self.b1 = SubtractSquareState('p2', current_total = 1)
        self.a2 = SubtractSquareState('p1', current_total = 2)
        self.b3 = SubtractSquareState('p2', current_total = 3)
        self.a4 = SubtractSquareState('p1', current_total = 4)
        self.b0 = SubtractSquareState('p2', current_total = 0)

        self.a0_node = GameStateNode(self.a0)
        self.b1_node = GameStateNode(self.b1)
        self.b1_node.children = [self.a0_node]
        self.a2_node = GameStateNode(self.a2)
        self.a2_node.children = [self.b1_node]
        self.b3_node = GameStateNode(self.b3)
        self.b3_node.children = [self.a2_node]
        self.b0_node = GameStateNode(self.b0)
        self.a4_node = GameStateNode(self.a4)
        self.a4_node.children = [self.b0_node, self.b3_node]

    def test1(self):
        root = GameStateNode(SubtractSquareState('p1', current_total = 4))
        root.grow()
        self.assertEqual(root, self.a4_node)
        

class TestNodeCount(unittest.TestCase):
    def setUp(self):
        self.b = SubtractSquareState('p1', current_total = 6)
        self.a = SubtractSquareState('p1', current_total = 0)
        self.c = SubtractSquareState('p1', current_total = 3)

        self.d = SubtractSquareState('p1', current_total = 13)
        self.e = SubtractSquareState('p1', current_total = 11)
	self.f = SubtractSquareState('p1', current_total = 1)
	self.g = SubtractSquareState('p1', current_total = 2)

    def test1(self):
        root = GameStateNode(self.b)
        root.grow()
        self.assertEqual(node_count(root), 13)

    def test2(self):
        root = GameStateNode(self.a)
        root.grow()
        self.assertEqual(node_count(root), 1)   

    def test3(self):
        root = GameStateNode(self.c)
        root.grow()
        self.assertEqual(node_count(root), 4)

    def test4(self):
        root = GameStateNode(self.d)
        root.grow()
        self.assertEqual(node_count(root), 147)

    def test5(self):
        root = GameStateNode(self.e)
        root.grow()
        self.assertEqual(node_count(root), 74)

    def test6(self):
        root = GameStateNode(self.f)
        root.grow()
        self.assertEqual(node_count(root), 2)

    def test7(self):
        root = GameStateNode(self.g)
        root.grow()
        self.assertEqual(node_count(root), 3)


class TestLeafCount(unittest.TestCase):
    def setUp(self):
        self.s = SubtractSquareState('p1', current_total = 6)
        self.c = SubtractSquareState('p1', current_total = 3)
        self.a = SubtractSquareState('p1', current_total = 0)

        self.d = SubtractSquareState('p1', current_total = 13)
        self.e = SubtractSquareState('p1', current_total = 11)
	self.f = SubtractSquareState('p1', current_total = 1)
	self.g = SubtractSquareState('p1', current_total = 2)

    def test1(self):
        root = GameStateNode(self.s)
        root.grow()
        self.assertEqual(leaf_count(root), 4)

    def test2(self):
        root = GameStateNode(self.c)
        root.grow()
        self.assertEqual(leaf_count(root), 1)

    def test3(self):
        root = GameStateNode(self.a)
        root.grow()
        self.assertEqual(leaf_count(root), 1)

    def test4(self):
        root = GameStateNode(self.d)
        root.grow()
        self.assertEqual(leaf_count(root), 43)

    def test5(self):
        root = GameStateNode(self.e)
        root.grow()
        self.assertEqual(leaf_count(root), 22)

    def test6(self):
        root = GameStateNode(self.f)
        root.grow()
        self.assertEqual(leaf_count(root), 1)

    def test7(self):
        root = GameStateNode(self.g)
        root.grow()
        self.assertEqual(leaf_count(root), 1)


class TestDistinctNodeCount(unittest.TestCase):
    def setUp(self):
        self.s = SubtractSquareState('p1', current_total = 6)
        self.c = SubtractSquareState('p1', current_total = 3)
        self.a = SubtractSquareState('p1', current_total = 0)

        self.d = SubtractSquareState('p1', current_total = 13)
        self.e = SubtractSquareState('p1', current_total = 11)
	self.f = SubtractSquareState('p1', current_total = 1)
	self.g = SubtractSquareState('p1', current_total = 2)

    def test1(self):
        root = GameStateNode(self.s)
        root.grow()
        self.assertEqual(distinct_node_count(root), 10)

    def test2(self):
        root = GameStateNode(self.c)
        root.grow()
        self.assertEqual(distinct_node_count(root), 4)

    def test3(self):
        root = GameStateNode(self.a)
        root.grow()
        self.assertEqual(distinct_node_count(root), 1)


    def test4(self):
        root = GameStateNode(self.d)
        root.grow()
        self.assertEqual(distinct_node_count(root), 24)

    def test5(self):
        root = GameStateNode(self.e)
        root.grow()
        self.assertEqual(distinct_node_count(root), 20)

    def test6(self):
        root = GameStateNode(self.f)
        root.grow()
        self.assertEqual(distinct_node_count(root), 2)

    def test7(self):
        root = GameStateNode(self.g)
        root.grow()
        self.assertEqual(distinct_node_count(root), 3)


class TestDistinctLeafCount(unittest.TestCase):
    def setUp(self):
        self.s = SubtractSquareState('p1', current_total = 6)
        self.c = SubtractSquareState('p1', current_total = 3)
        self.a = SubtractSquareState('p1', current_total = 0)

        self.d = SubtractSquareState('p1', current_total = 13)
        self.e = SubtractSquareState('p1', current_total = 11)
	self.f = SubtractSquareState('p1', current_total = 1)
	self.g = SubtractSquareState('p1', current_total = 2)

    def test1(self):
        root = GameStateNode(self.s)
        root.grow()
        self.assertEqual(distinct_leaf_count(root), 2)

    def test2(self):
        root = GameStateNode(self.a)
        root.grow()
        self.assertEqual(distinct_leaf_count(root), 1)

    def test3(self):
        root = GameStateNode(self.c)
        root.grow()
        self.assertEqual(distinct_leaf_count(root), 1)

    def test4(self):
        root = GameStateNode(self.d)
        root.grow()
        self.assertEqual(distinct_leaf_count(root), 2)

    def test5(self):
        root = GameStateNode(self.e)
        root.grow()
        self.assertEqual(distinct_leaf_count(root), 2)

    def test6(self):
        root = GameStateNode(self.f)
        root.grow()
        self.assertEqual(distinct_leaf_count(root), 1)

    def test7(self):
        root = GameStateNode(self.g)
        root.grow()
        self.assertEqual(distinct_leaf_count(root), 1)


class TestBranchingStats(unittest.TestCase):
    def setUp(self):
        self.s = SubtractSquareState('p1', current_total = 6)
        self.c = SubtractSquareState('p1', current_total = 3)
        self.a = SubtractSquareState('p1', current_total = 0)

        self.d = SubtractSquareState('p1', current_total = 13)
        self.e = SubtractSquareState('p1', current_total = 11)
	self.f = SubtractSquareState('p1', current_total = 1)
	self.g = SubtractSquareState('p1', current_total = 2)

    def test1(self):
        root = GameStateNode(self.s)
        root.grow()
        self.assertEqual(branching_stats(root), {0: 4, 1: 6, 2: 3})

    def test2(self):
        root = GameStateNode(self.c)
        root.grow()
        self.assertEqual(branching_stats(root), {0: 1, 1: 3})

    def test3(self):
        root = GameStateNode(self.a)
        root.grow()
        self.assertEqual(branching_stats(root), {0: 1})

    def test4(self):
        root = GameStateNode(self.d)
        root.grow()
        self.assertEqual(branching_stats(root), {0: 43, 1: 68, 2: 30, 3: 6})

    def test5(self):
        root = GameStateNode(self.e)
        root.grow()
        self.assertEqual(branching_stats(root), {0: 22, 1: 34, 2: 15, 3: 3})

    def test6(self):
        root = GameStateNode(self.f)
        root.grow()
        self.assertEqual(branching_stats(root), {0: 1, 1: 1})

    def test7(self):
        root = GameStateNode(self.g)
        root.grow()
        self.assertEqual(branching_stats(root), {0: 1, 1: 2})

class TestOutcomeCounts(unittest.TestCase): 
    def setUp(self):
        self.s = SubtractSquareState('p1', current_total = 6)
        self.c = SubtractSquareState('p1', current_total = 3)
        self.a1 = SubtractSquareState('p1', current_total = 0)
        self.a2 = SubtractSquareState('p2', current_total = 0)
        self.b = SubtractSquareState('p1', current_total = 9)

        self.d1 = SubtractSquareState('p1', current_total = 13)
        self.e1 = SubtractSquareState('p1', current_total = 11)
        self.d2 = SubtractSquareState('p2', current_total = 13)
        self.e2 = SubtractSquareState('p2', current_total = 11)

    def test1(self):
        root = GameStateNode(self.s)
        root.grow()
        self.assertEqual(outcome_counts(root), [3, 1, 0])

    def test2(self):
        root = GameStateNode(self.a1)
        root.grow()
        self.assertEqual(outcome_counts(root), [0, 1, 0])

    def test3(self):
        root = GameStateNode(self.a2)
        root.grow()
        self.assertEqual(outcome_counts(root), [1, 0, 0])

    def test4(self):
        root = GameStateNode(self.c)
        root.grow()
        self.assertEqual(outcome_counts(root), [1, 0, 0])

    def test5(self):
        root = GameStateNode(self.b)
        root.grow()
        self.assertEqual(outcome_counts(root), [5, 6, 0])

    def test6(self):
        root = GameStateNode(self.d1)
        root.grow()
        self.assertEqual(outcome_counts(root), [27, 16, 0])

    def test7(self):
        root = GameStateNode(self.e1)
        root.grow()
        self.assertEqual(outcome_counts(root), [14, 8, 0])

    '''
    #These Are Fucked
    def test8(self):
        root = GameStateNode(self.d2)
        root.grow()
        self.assertEqual(outcome_counts(root), [27, 16, 0])

    def test9(self):
        root = GameStateNode(self.e2)
        root.grow()
        self.assertEqual(outcome_counts(root), [14, 8, 0])
    '''

class TestGameLengths(unittest.TestCase): 
    def setUp(self):
        self.s = SubtractSquareState('p1', current_total = 6)
        self.c = SubtractSquareState('p1', current_total = 3)
        self.a = SubtractSquareState('p1', current_total = 0)
        self.b = SubtractSquareState('p1', current_total = 9)

        self.d = SubtractSquareState('p1', current_total = 13)
        self.e = SubtractSquareState('p1', current_total = 11)

    def test1(self):
        root = GameStateNode(self.s)
        root.grow()
        self.assertEqual(game_lengths(root), {6: 1, 3: 3})

    def test2(self):
        root = GameStateNode(self.a)
        root.grow()
        self.assertEqual(game_lengths(root), {0: 1})

    def test3(self):
        root = GameStateNode(self.b)
        root.grow()
        self.assertEqual(game_lengths(root), {1: 1, 3: 3, 6: 6, 9: 1})

    def test4(self):
        root = GameStateNode(self.c)
        root.grow()
        self.assertEqual(game_lengths(root), {3: 1})

    def test5(self):
        root = GameStateNode(self.d)
        root.grow()
        self.assertEqual(game_lengths(root),  {2: 2, 4: 4, 5: 5, 7: 21, 10: 10, 13: 1})

    def test6(self):
        root = GameStateNode(self.e)
        root.grow()
        self.assertEqual(game_lengths(root), {8: 8, 11: 1, 3: 3, 5: 10})


class TestGameDescription(unittest.TestCase):
    def setUp(self):
        self.s = SubtractSquareState('p1', current_total = 6)
        self.a = SubtractSquareState('p2', current_total = 0)
        self.b = SubtractSquareState('p1', current_total = 9)
	self.f = SubtractSquareState('p1', current_total = 1)
	self.g = SubtractSquareState('p1', current_total = 2)

    def test1(self):
        root = GameStateNode(self.s)
        root.grow()
        self.assertEqual(game_descriptions(root), ['p1:6 -> p2:2 -> p1:1 -> p2:0 = p1 wins!', 'p1:6 -> p2:5 -> p1:1 -> p2:0 = p1 wins!', 'p1:6 -> p2:5 -> p1:4 -> p2:0 = p1 wins!', 'p1:6 -> p2:5 -> p1:4 -> p2:3 -> p1:2 -> p2:1 -> p1:0 = p2 wins!'])


    def test2(self):
        root = GameStateNode(self.a)
        root.grow()
        self.assertEqual(game_descriptions(root), ['p2:0 = p1 wins!'])

    def test3(self):
        root = GameStateNode(self.f)
        root.grow()
        self.assertEqual(game_descriptions(root), ['p1:1 -> p2:0 = p1 wins!'])

    def test4(self):
        root = GameStateNode(self.g)
        root.grow()
        self.assertEqual(game_descriptions(root), ['p1:2 -> p2:1 -> p1:0 = p2 wins!'])


if __name__ == '__main__':
    unittest.main(exit=False)
